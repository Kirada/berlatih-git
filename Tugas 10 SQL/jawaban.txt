1. Membuat Database
CREATE DATABASE myshop;

2. Membuat table

tabel user
CREATE TABLE USER( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), email varchar(255), PASSWORD varchar(255) );

tabel categories

CREATE TABLE categories( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) );

tabel items
CREATE TABLE items( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), description varchar(255), price int(10), stock int(10), category_id int(8), FOREIGN KEY (category_id) REFERENCES categories(id) );

3. Insert Data
- tabel user 
INSERT INTO user(name, email, PASSWORD) VALUES("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com" ,"jenita123");

-tabel categories
INSERT INTO categories(name) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded");

-tabel items
INSERT INTO items(name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. Ambil data
a.SELECT id, name, email FROM user;

b.mengambil data
-SELECT * FROM items WHERE price >= 1000000;
-SELECT * FROM items WHERE name LIKE "%Uniklo%";

c. join
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori FROM items INNER JOIN categories ON items.category_id = categories.id;

5.update data
UPDATE items SET price= 2500000 WHERE id = 1;