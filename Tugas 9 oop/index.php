<?php

require ('animal.php');
require_once ('Frog.php');
require_once ('ape.php');

$sheep = new animal ("shaun");

echo  "Nama : $sheep->name <br>"; // "shaun"
echo "legs : $sheep->legs <br>"; // 4
echo "cold blooded: $sheep->cold_blooded <br> <br>"; // "no"

$Frog = new Frog ("buduk");

echo  "Nama : $Frog->name <br>"; // "shaun"
echo "legs : $Frog->legs <br>"; // 4
echo "cold blooded: $Frog->cold_blooded <br>"; // "no"
echo $Frog -> jump();

$ape = new ape ("kera sakti");

echo  "Nama : $ape->name <br>"; // "shaun"
echo "legs : $ape->legs <br>"; // 4
echo "cold blooded: $ape->cold_blooded <br>"; // "no"
echo $ape -> yell();
?>